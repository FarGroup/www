<br/>


<h2><?php print $dl_stable_builds;?></h2>

<div>
    <ul>
	<li>
		<?php include("files/FarW."."32".".php");?>
	    <b>Far Manager v<?php print $farnew_major;?>.<?php print $farnew_minor;?> build <?php print $farnew_build;?> <?php print $farnew_platform;?></b> (<?php print $farnew_date;?>)
		<br/><br/>
	    <a class="body_link" href="files/<?php print $farnew_arc;?>">
		<img border="0" src="img/archive.<?php print $lang;?>.png" alt="download"></a>
	    &nbsp;
	    <a class="body_link" href="files/<?php print $farnew_msi;?>">
		<img border="0" src="img/msi.<?php print $lang;?>.png" alt="download"></a>
	    &nbsp;
	    <a class="body_link" href="files/<?php print $farnew_pdb;?>">
		<img border="0" src="img/pdb.<?php print $lang;?>.png" alt="download"></a>
	    <br/>
	    <br/>
	</li>
	<li>
		<?php include("files/FarW."."64".".php");?>
	    <b>Far Manager v<?php print $farnew_major;?>.<?php print $farnew_minor;?> build <?php print $farnew_build;?> <?php print $farnew_platform;?></b> (<?php print $farnew_date;?>)
		<br/><br/>
	    <a class="body_link" href="files/<?php print $farnew_arc;?>">
		<img border="0" src="img/archive.<?php print $lang;?>.png" alt="download"></a>
	    &nbsp;
	    <a class="body_link" href="files/<?php print $farnew_msi;?>">
		<img border="0" src="img/msi.<?php print $lang;?>.png" alt="download"></a>
	    &nbsp;
	    <a class="body_link" href="files/<?php print $farnew_pdb;?>">
		<img border="0" src="img/pdb.<?php print $lang;?>.png" alt="download"></a>
	    <br/>
	    <br/>
	</li>
	</ul>
</div>
<h2><?php print $dl_nightly_builds;?>&nbsp;&nbsp;<a class="body_comment" href="https://github.com/FarGroup/FarManager/raw/master/far/<?php print $dl_changelog2_file;?>"><?php print $dl_full_changelog;?></a></h2>
<div>
    <ul>
	<li>
	    <?php include("nightly/FarW."."32".".php");?>
	    <b>Far Manager v<?php print $farnew_major;?>.<?php print $farnew_minor;?> build <?php print $farnew_build;?> <?php print $farnew_platform;?></b> (<?php print $farnew_date;?>)
	    <div class="body_comment">
		<?php print $dl_last_change;?>: <?php print $farnew_lastchange;?>
	    </div>
	    <br/>
	    <a class="body_link" href="nightly/<?php print $farnew_arc;?>">
		<img border="0" src="img/archive.<?php print $lang;?>.png" alt="download"></a>
	    &nbsp;
	    <a class="body_link" href="nightly/<?php print $farnew_msi;?>">
		<img border="0" src="img/msi.<?php print $lang;?>.png" alt="download"></a>
	    &nbsp;
	    <a class="body_link" href="nightly/<?php print $farnew_pdb;?>">
		<img border="0" src="img/pdb.<?php print $lang;?>.png" alt="download"></a>
	    <br/>
	    <br/>
	</li>
	<li>
		<?php include("nightly/FarW."."64".".php");?>
	    <b>Far Manager v<?php print $farnew_major;?>.<?php print $farnew_minor;?> build <?php print $farnew_build;?> <?php print $farnew_platform;?></b> (<?php print $farnew_date;?>)
	    <div class="body_comment">
		<?php print $dl_last_change;?>: <?php print $farnew_lastchange;?>
	    </div>
	    <br/>
	    <a class="body_link" href="nightly/<?php print $farnew_arc;?>">
		<img border="0" src="img/archive.<?php print $lang;?>.png" alt="download"></a>
	    &nbsp;
	    <a class="body_link" href="nightly/<?php print $farnew_msi;?>">
		<img border="0" src="img/msi.<?php print $lang;?>.png" alt="download"></a>
	    &nbsp;
	    <a class="body_link" href="nightly/<?php print $farnew_pdb;?>">
		<img border="0" src="img/pdb.<?php print $lang;?>.png" alt="download"></a>
	    <br/>
	    <br/>
	</li>
	</ul>
</div>

<div><h2><?php print $dl_history_msg;?></h2>
<div>
    <ul>
	<li><a class="body_link" href="history.php">1996 - 2011</a></li>
    </ul>
</div>
