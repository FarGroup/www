<div>
	<h2>����� ������������ ������������ �������</h2>

	<p>�������� Far Manager ��������� ����� ������� �� ������ ���������, ��� �������� � ��� � �� �������� � �������� ������ ������������. ������� ���������� ��������� ����������� Far Manager, �������� �� � �����������.

	<p>�� �������� � ����������� � ����������, ����� �������� ������ ��������� �� ���� ������������:

	<ul>
	<li>���������� ����������, ��� ������������� � ��, ��� � ��������
	<li>��������� ���������� � �������� ������� ��������
	<li>������ � FTP-��������� (� ���������� ������� ����� ��������� ���� ������, �������������� �������� � ������)
	<li>����� � ������ �������� ������������ �� ��������� ������ � ����������� ���������� ���������.
	<li>�������� �������������� ����� ������ � ������������ ������������� ������� ��������� ����� �� �������� ����������� � ��������
	<li>NNTP/SMTP/POP3/IMAP4 ������� � �������� ��������� �� �������
	<li>������ ��� ������������� �������� ���������� ������
	<li>������������� ������� � ������ ������������ ������� ������
	<li>����������� � ���������� �������
	<li>���������� ������������ ��������� �� ��������� ��� �� ������� ��
	<li>�������������� ���� � ��������� � ������ � ���������
	<li>�������������� ���������� ������� Windows
	<li>�������� � ��������� ������� Windows
	<li>������������ ����������� � ������� � �������, �������� ���������� ������ � FIDO
	<li>����������� � ������������� ������ � ������� UUE
	<li>���������� ���������� WinAmp � ����������� ������������ MP3-������
	<li>��������� Quake PAK ������
	<li>������ � ���������� ��������� ����� ODBC + ������ � ��������� ORACLE ����� OCI.
	<li>���������� ������� RAS
	<li>������ ������� �������� (������������, ����������� � ����.) ��� �������������� ������� � ��������� Far
	<li>����������� ����������� ������ ������� Windows (.hlp � .chm)
	<li>������������ � ������� �������������
	<li>��������� ����
	<li>������� �������� ���������� ��� ��������� ������ � ��������� Far
	<li>���������� �������� ������� ����������� � ������ ������...
	</ul>

	<h2>��� ������� �������?</h2>

	<p>����������� ��� �������, ������� ������������ Far Manager � ��� �������, ����������, ��������� �� �������� ��������� �������������. � �������� ���������� ����������, ������� ����� ������������ ��� ������ ���������� ��������, ����� ���������������:

	<ul>
		<li><b>�����</b><br>
		<a class="body_link" href="https://forum.farmanager.com/viewforum.php?f=11" target="_blank">https://forum.farmanager.com</a><br><br>

		<li><b>���� PlugRinG</b><br>
		<a class="body_link" href="https://plugring.farmanager.com" target="_blank">https://plugring.farmanager.com</a><br><br>
	</ul>
</div>
