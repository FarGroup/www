<script type="text/javascript">
		$(document).ready(function(){
			$("#gallery a").lightbox({
		        loopImages: true,
		        imageClickClose: false,
		        disableNavbarLinks: true,
			    fitToScreen: true,
			    strings : { 
			    	image : '����������� ', 
			    	of : ' �� ', 
					prevLinkTitle: '����������',
					nextLinkTitle: '���������',
					prevLinkText:  '&laquo; ����������',
					nextLinkText:  '��������� &raquo;',
					closeTitle: '��������� ��������'
			    	}
		    });

		});
</script>

<div id="gallery" align="center">
	<a href="img/screen/altf7.png" rel="screenshots" title="����� ������"><img class="bordered" src="img/thumb/thumb_altf7.png" alt="����� ������"></a>
	<a href="img/screen/ftp.png" rel="screenshots" title="FTP"><img class="bordered" src="img/thumb/thumb_ftp.png" alt="ftp"></a>
	<a href="img/screen/editor.png" rel="screenshots" title="��������"><img class="bordered" src="img/thumb/thumb_editor.png" alt="��������"></a>
	<a href="img/screen/viewer.png" rel="screenshots" title="��������"><img class="bordered" src="img/thumb/thumb_viewer.png" alt="��������"></a>
	<a href="img/screen/altf72.png" rel="screenshots" title="����� ������"><img class="bordered" src="img/thumb/thumb_altf72.png" alt="����� ������"></a>
	<a href="img/screen/ctrll.png" rel="screenshots" title="������ ����������"><img class="bordered" src="img/thumb/thumb_ctrll.png" alt="������ ����������"></a>
	<a href="img/screen/ctrlq.png" rel="screenshots" title="������� ��������"><img class="bordered" src="img/thumb/thumb_ctrlq.png" alt="������� ��������"></a>
	<a href="img/screen/colors.png" rel="screenshots" title="��������� ������"><img class="bordered" src="img/thumb/thumb_colors.png" alt="��������� ������"></a>
	<a href="img/screen/width.png" rel="screenshots" title="��������� ������� ������"><img class="bordered" src="img/thumb/thumb_width.png" alt="��������� ������� ������"></a>
	<a href="img/screen/tree.png" rel="screenshots" title="������"><img class="bordered" src="img/thumb/thumb_tree.png" alt="������"></a>
	<a href="img/screen/f1.png" rel="screenshots" title="�������"><img class="bordered" src="img/thumb/thumb_f1.png" alt="�������"></a>
	<a href="img/screen/elevation.png" rel="screenshots" title="Elevation"><img class="bordered" src="img/thumb/thumb_elevation.png" alt="elevation"></a>
</div>
