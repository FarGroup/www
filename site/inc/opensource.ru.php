<div>
<p>������� � ������ 1.80 (26 ������� 2007 �.) Far Manager ���������������� ������ � �������� ����� ��� <a class="body_link" href="license.php?l=ru">����������������&nbsp;���������&nbsp;BSD</a>.
</p>

<h3>�������� �������:</h3>
<a class="body_link" href="https://github.com/FarGroup/FarManager">https://github.com/FarGroup/FarManager</a><br><br>

<h3>��� ������:</h3>
<a class="body_link" href="https://bugs.farmanager.com/">https://bugs.farmanager.com/</a><br><br>

<h3>�������� ��� ������������� (�������������):</h3>
<a class="body_link" href="https://groups.google.com/group/fardev">https://groups.google.com/group/fardev</a><br>
<a class="body_link" href="mailto:fardev@googlegroups.com">fardev@googlegroups.com</a><br><br>

<h3>�������� ��� ������������� (������������):</h3>
<a class="body_link" href="https://groups.google.com/group/fardeven">https://groups.google.com/group/fardeven</a><br>
<a class="body_link" href="mailto:fardeven@googlegroups.com">fardeven@googlegroups.com</a><br><br>

<h3>�������� ��������:</h3>
<a class="body_link" href="https://groups.google.com/group/farcommits">https://groups.google.com/group/farcommits</a><br>
<a class="body_link" href="mailto:farcommits@googlegroups.com">farcommits@googlegroups.com</a><br><br>

</div>
